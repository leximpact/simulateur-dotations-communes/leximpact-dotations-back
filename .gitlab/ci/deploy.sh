#!/bin/bash

# USAGE: to be copied by the CI to the integration or production container and executed there

# stop at first error
set -e

echo "Stop all services..."
systemctl --user stop dotations-back-integ.service

echo "Install deps..."
# dependencies in '.venv/' supposing that the virtual environment have been pre-configured with:
# poetry config virtualenvs.in-project true
rm -rf .venv
/home/leximpact/.local/bin/poetry run pip install dist/*.whl

echo "Restart service..."
systemctl --user start dotations-back-integ.service

echo "Deployment done! 🎉"
