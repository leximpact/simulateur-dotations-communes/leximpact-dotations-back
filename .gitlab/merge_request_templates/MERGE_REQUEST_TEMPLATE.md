Merci de contribuer à leximpact-dotations-back ! Effacez cette ligne ainsi que, pour chaque ligne ci-dessous, les cas ne correspondant pas à votre contribution :)

* Ajout d'une fonctionnalité. | Évolution **non rétro-compatible**. | Amélioration technique. | Correction d'un crash. | Changement mineur.
* Période [des dotations] : toutes. | jusqu'au JJ/MM/AAAA. | à partir du JJ/MM/AAAA.
* Détails :
  - Description de la fonctionnalité ajoutée ou du nouveau comportement adopté.
  - Cas dans lesquels une erreur était constatée.
  - Eventuel guide de migration pour les réutilisations.

- - - -

Ces changements (effacez les lignes ne correspondant pas à votre cas) :

- Modifient l'API de leximpact-dotations-back (par exemple changement du format de requête de l'API web).
- Ajoutent une fonctionnalité (par exemple ajout d'une nouvelle année de calcul des dotations).
- Corrigent ou améliorent un calcul déjà existant.
- Modifient des éléments non fonctionnels de ce dépôt (par exemple modification du README).
