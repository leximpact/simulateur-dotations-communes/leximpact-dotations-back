# CHANGELOG

### 1.0.1 [!11](https://git.leximpact.dev/leximpact/simulateur-dotations-communes/leximpact-dotations-back/-/merge_requests/11)

* Évolution des données de la simulation.
* Période : 2024
* Détails :
  * Met à jour les données 2024 nettoyées `data/criteres_repartition_2024.csv`
    * Ajoute la dotation commune nouvelle créée en 2024 aux montants des dotations à conserver
    * Ajoute la garantie DSU aux montants à conserver
  * Met à jour la dépendance `openfisca-france-dotations-locales`

# 1.0.0 [!7](https://git.leximpact.dev/leximpact/simulateur-dotations-communes/leximpact-dotations-back/-/merge_requests/7)

* Évolution de la simulation.
* Période : 2023 pour simulation 2024
* Détails :
  * Prend en compte les données de l'année précédente pour leurs montants de dotations 2023
    * Ajoute les données de critères 2023 brutes `data/raw/criteres_repartition_2023_raw.csv`
    * Initie le mapping entre colonnes DGCL et variables openfisca `leximpact_dotations_back/mapping/criteres_dgcl_2023.py`
    * Prend en comptes les données 2023 dans le script de nettoyage `data/raw/data_cleanup.py`
    * Ajoute le CSV des données 2023 résultats du nettoyage `data/criteres_repartition_2023.csv`
    * Construit le DataFrame des données 2023
  * Ajoute des montants 2023 à l'intialisation de la simulation 2024 par `leximpact_dotations_back/calculate.py`
    * Arrête l'application sur `VariableNotFoundError` en cas d'erreur à l'initialisation de la simulation
  * Précise les cas particuliers pris en compte dans l'adaptation des critères 2024
  * Précise des types de données 2024 levant des erreurs ou zéros à la simulation
    * Complète le mapping `leximpact_dotations_back/mapping/criteres_dgcl_2024.py`

### 0.2.3 [!10](https://git.leximpact.dev/leximpact/simulateur-dotations-communes/leximpact-dotations-back/-/merge_requests/10)

* Correction d'un crash technique.
* Détails :
  * Complète la correction du job `deploy-integ` en CI (en complément à la v.`0.2.2`)
  * Corrige le chemin d'accès aux wheels (`/dist`) sur le serveur d'intégration

### 0.2.2 [!9](https://git.leximpact.dev/leximpact/simulateur-dotations-communes/leximpact-dotations-back/-/merge_requests/9)
> Correction complétée en `0.2.3`

* Correction d'un crash technique.
* Détails :
  * Corrige le job `deploy-integ` en CI
  * Sur le serveur d'intégration, nettoie la wheel précédente avant de déployer la nouvelle pour éviter la tentative d'installation de multiples `leximpact-dotations-back`

### 0.2.1 [!8](https://git.leximpact.dev/leximpact/simulateur-dotations-communes/leximpact-dotations-back/-/merge_requests/8)

* Amélioration technique.
* Détails :
  * Ajoute un script de déploiement de la wheel de `leximpact-dotations-back` sur le conteneur d'intégration `.gitlab/ci/deploy.sh`
  * Ajoute un script d'exécution de l'API web à partir de la wheel `.gitlab/ci/launch_api.py`
  * Ajoute un job `deploy-integ` pour le déploiement manuel de `leximpact-dotations-back` en intégration

## 0.2.0 [!3](https://git.leximpact.dev/leximpact/simulateur-dotations-communes/leximpact-dotations-back/-/merge_requests/3)

* Ajout d'une fonctionnalité.
* Période : 2024 (compatible 2021 à 2023)
* Détails :
  * Construit le DataFrame de simulation 2024 sur la base des traitements de données réalisés de 2021 à 2023 :
    * Corrige le format des données `data/criteres_repartition_2024.csv` via `data/raw/data_cleanup.py:clean_data_values`
    * Définit le mapping données/openfisca 2024 dans `leximpact_dotations_back/mapping/criteres_dgcl_2024.py`
    * Ajoute le sous-module `leximpact_dotations_back.data_building` avec les algorithmes sur données des années passées
    * Modifie a minima les algorithmes passés pour permettre l'exécution sans crash et ragroupe les contantes comparables par fichier
  * Déplace le mapping 2022 dans les tests
  * Initie des tests de `mapping` et `data_building` 2024
  * Définit le logger de `main.py` comme logger par défaut du module `leximpact_dotations_back`
  * Limite la version de Python maximale à `3.11` en cohérence avec la limite `openfisca-core`

### 0.1.3 [!6](https://git.leximpact.dev/leximpact/simulateur-dotations-communes/leximpact-dotations-back/-/merge_requests/6)

* Amélioration technique.
* Détails :
  * Complète la configuration de l'intégration continue dont le déploiement depuis la branche `main` et environnement `integration`
  * Ajoute la création de [release GitLab](https://git.leximpact.dev/leximpact/simulateur-dotations-communes/leximpact-dotations-back/-/releases) pour permettre la création de [tag GIT](https://git.leximpact.dev/leximpact/simulateur-dotations-communes/leximpact-dotations-back/-/tags)
  * Ajoute la [publication sur PyPi](https://pypi.org/project/leximpact-dotations-back/)
  * Initie un fichier `CONTRIBUTING.md`

### 0.1.2 [!5](https://git.leximpact.dev/leximpact/simulateur-dotations-communes/leximpact-dotations-back/-/merge_requests/5)

* Amélioration technique
* Détails : 
  * Ajoute le texte de la LICENSE
  * Met à jour la description de la librairie dans `pyproject.toml` (contact, licence, classification)

### 0.1.1  [!4](https://git.leximpact.dev/leximpact/simulateur-dotations-communes/leximpact-dotations-back/-/merge_requests/4)

* Amélioration technique.
* Détails :
  * Configure les intéractions avec GitLab :
    * Initie l'intégration continue pour l'installation, la vérification de style et le test
    * Ajoute les templates d'ouverture d'issue et de merge request
  * Configure les vérifications de style du code avec `flake8` et `autopep8`
    * Permet la configuration de `flake8` dans `pyproject.toml` de poetry grâce à la librairie `flake8-pyproject`
    * Corrige le style des fichiers Python
    * Documente les commandes dans le `README`

## 0.1.0 [!2](https://git.leximpact.dev/leximpact/simulateur-dotations-communes/leximpact-dotations-back/-/merge_requests/2)

* Ajout d'une fonctionnalité.
* Période : 2024
* Détails :
  * Crée le CSV des données de base de la simulation des dotations 2024 :
    * Ajoute le script `data/raw/data_cleanup.py` de nettoyage des données CSV brutes de critères DGCL
    * Ajoute les données CSV brutes de critères DGCL `data/raw/criteres_repartition_2024_raw.csv` (entrée du script)
    * Ajoute les données CSV `data/criteres_repartition_2024.csv` (sortie du script ; base de la simulation)
    * Initie le module `leximpact_dotations_back.mapping`
    * Initie une configuration des traces

### 0.0.1 [!1](https://git.leximpact.dev/leximpact/simulateur-dotations-communes/leximpact-dotations-back/-/merge_requests/1)

* Ajout d'une fonctionnalité.
* Détails :
  * Crée la librairie `leximpact_dotations_back`
  * Initie la création d'une simulation sur la base d'`openfisca_france_dotations_locales`
  * Crée une API web avec [FastAPI](https://fastapi.tiangolo.com)
    * Crée un endpoint de bienvenue `/`
    * Crée un endpoint d'information sur des versions de librairies clefs `/dependencies`
  * Initie les tests unitaires avec [pytest](https://docs.pytest.org/en/7.4.x/)
  * Crée une documentation minimale sur tout le périmètre développé
