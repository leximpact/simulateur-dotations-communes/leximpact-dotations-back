# Contribuer à leximpact-dotations-back

Avant tout, merci de votre volonté de contribuer au dépôt `leximpact-dotations-back` !

Afin de faciliter son utilisation et d'améliorer la qualité du code, les contributions suivent certaines règles décrites dans ce fichier.

## Format des versions et du CHANGELOG

Les évolutions de `leximpact-dotations-back` doivent pouvoir être comprises par des utilisateurs ne disposant pas du contexte complet de l'application. C'est pourquoi nous faisons le choix :

* D'un [versionnement sémantique](https://semver.org/lang/fr/) de l'application où l'évaluation des changements majeurs se fait au regard des évolutions de l'API web.
* De l'existence d'un [CHANGELOG.md](./CHANGELOG.md), rédigé en français, pour la description des évolutions. Celui-ci se doit être le plus explicite possible.
