# Loading DGCL raw CSV "criteres_repartition_YYYY_raw.csv"
# and creating new "criteres_repartition_YYYY.csv" with cleaned data format for simulation.
#
# USAGE:
# leximpact-dotations-back$ poetry run python data/raw/data_cleanup.py

import sys
import traceback

from os.path import dirname, join
from pandas import DataFrame, isna, read_csv, Series, to_numeric

from leximpact_dotations_back import logger
from leximpact_dotations_back.mapping.valeurs_dgcl import DGCL_VALUE__NON_COMMUNIQUE, DGCL_VALUE__NON_DISPONIBLE_BEFORE_2024, DGCL_VALUE__NON_DISPONIBLE_2024, DGCL_VALUE__STRING_NON_BREAKING_SPACE
from leximpact_dotations_back.mapping.criteres_dgcl_2023 import (
    DOTATION_FORFAITAIRE_DTYPE_STR as DOTATION_FORFAITAIRE_DTYPE_STR_2023,
    DSU_MONTANT_DTYPE_STR as DSU_MONTANT_DTYPE_STR_2023,
    DSU_PART_SPONTANEE_DTYPE_STR as DSU_PART_SPONTANEE_DTYPE_STR_2023,
    DSU_PART_AUGMENTATION_DTYPE_STR as DSU_PART_AUGMENTATION_DTYPE_STR_2023,
    DSR_FRACTION_CIBLE_PART_POTENTIEL_FINANCIER_PAR_HABITANT_DTYPE_STR as DSR_FRACTION_CIBLE_PART_POTENTIEL_FINANCIER_PAR_HABITANT_DTYPE_STR_2023,
    DSR_FRACTION_CIBLE_PART_LONGUEUR_VOIRIE_DTYPE_STR as DSR_FRACTION_CIBLE_PART_LONGUEUR_VOIRIE_DTYPE_STR_2023,
    DSR_FRACTION_CIBLE_PART_ENFANTS_DTYPE_STR as DSR_FRACTION_CIBLE_PART_ENFANTS_DTYPE_STR_2023,
    DSR_FRACTION_CIBLE_PART_POTENTIEL_FINANCIER_PAR_HECTARE_DTYPE_STR as DSR_FRACTION_CIBLE_PART_POTENTIEL_FINANCIER_PAR_HECTARE_DTYPE_STR_2023,
    DSR_FRACTION_CIBLE_DTYPE_STR as DSR_FRACTION_CIBLE_DTYPE_STR_2023,
    DSR_FRACTION_PEREQUATION_PART_POTENTIEL_FINANCIER_PAR_HABITANT_DTYPE_STR as DSR_FRACTION_PEREQUATION_PART_POTENTIEL_FINANCIER_PAR_HABITANT_DTYPE_STR_2023,
    DSR_FRACTION_PEREQUATION_PART_LONGUEUR_VOIRIE_DTYPE_STR as DSR_FRACTION_PEREQUATION_PART_LONGUEUR_VOIRIE_DTYPE_STR_2023,
    DSR_FRACTION_PEREQUATION_PART_ENFANTS_DTYPE_STR as DSR_FRACTION_PEREQUATION_PART_ENFANTS_DTYPE_STR_2023,
    DSR_FRACTION_PEREQUATION_PART_POTENTIEL_FINANCIER_PAR_HECTARE_DTYPE_STR as DSR_FRACTION_PEREQUATION_PART_POTENTIEL_FINANCIER_PAR_HECTARE_DTYPE_STR_2023,
    DSR_MONTANT_ELIGIBLE_FRACTION_BOURG_CENTRE_DTYPE_STR as DSR_MONTANT_ELIGIBLE_FRACTION_BOURG_CENTRE_DTYPE_STR_2023,
    DSR_FRACTION_BOURG_CENTRE_DTYPE_STR as DSR_FRACTION_BOURG_CENTRE_DTYPE_STR_2023,
    DSR_FRACTION_PEREQUATION_DTYPE_STR as DSR_FRACTION_PEREQUATION_DTYPE_STR_2023
)

from leximpact_dotations_back.mapping.criteres_dgcl_2024 import (
    EFFORT_FISCAL_DTYPE_STR,
    PART_POPULATION_CANTON_DTYPE_STR, POPULATION_INSEE_DTYPE_STR, POPULATION_DGF_DTYPE_STR,
    SUPERFICIE_DTYPE_STR,
    POTENTIEL_FINANCIER_DTYPE_STR, POTENTIEL_FINANCIER_PAR_HABITANT_DTYPE_STR, POTENTIEL_FISCAL_DTYPE_STR,
    REVENU_TOTAL_DTYPE_STR, RECETTES_REELLES_FONCTIONNEMENT_DTYPE_STR, DSR_C_INDICE_SYNTHETIQUE_DTYPE_STR,
    DF_POPULATION_DGF_MAJOREE_DTYPE_STR,
    DSR_BC_POPULATION_DGF_AGGLO_DTYPE_STR, DSR_BC_POPULATION_DGF_DEP_AGGLO_DTYPE_STR, DSR_BC_MONTANT_COMMUNE_ELIGIBLE_DTYPE_STR, DSR_BC_IS_CHEF_LIEU_DEP_AGGLO_DTYPE_STR,
    DSR_PRQ_PART_PFI_DTYPE_STR, DSR_PRQ_LONGUEUR_VOIRIE_DTYPE_STR, DSR_PRQ_POPULATION_ENFANTS_DTYPE_STR,
    DSR_C_RANG_INDICE_SYNTHETIQUE_DTYPE_STR,
    DSU_NOMBRE_BENEFICIAIRES_AIDES_LOGEMENT_DTYPE_STR,
    DSU_NOMBRE_LOGEMENTS_DTYPE_STR,
    DSU_NOMBRE_LOGEMENTS_SOCIAUX_DTYPE_STR,
    DSU_POPULATION_QPV_DTYPE_STR,
    DSU_POPULATION_ZFU_DTYPE_STR,
    DSU_MONTANT_GARANTIE_NOTIFIEE_DTYPE_STR,
    TOUTE_DOTATION_MONTANT_DTYPE_STR,
    montants_dotations_2024
)


FILES_DIRPATH = dirname(__file__)  # this script + raw data directory
OUTPUT_RELATIVE_DIRPATH = ".."
logger.debug(FILES_DIRPATH)

CRITERES_FILENAME_PREFIX = "criteres_repartition_"
CRITERES_RAW_FILENAME_SUFFIX = "_raw.csv"

OUTPUT_VALUE__EMPTY = ""
OUTPUT_VALUE__NEUTRAL_NUMERICAL = 0  # TODO use openfisca default value according to variable?
OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR = "0"
DEBUG_DISPLAYED_ROWS_NUMBER = 5

OUTPUT_VALUE__EMPTY = ""
DEBUG_DISPLAYED_ROWS_NUMBER = 5


def drop_separation_columns(data: DataFrame):
    '''
    Remove separation columns between groups of critères.
    In 2024, their first cell is empty while all the following cells contain a hyphen.
    '''
    data_without_separation_columns = data.copy()

    dropped_columns_names = []
    for index, column_name in enumerate(data.columns):
        column_content = data.iloc[:, index]
        # row 0 is empty, row 1 contains the main titles (dotation), row 2 contains the subtitles (critère)
        # but for separation columns where the cell on row 1 is NaN and the cell on row 2 (and next rows) is a hyphen
        if (isna(column_content[0]) and column_content[1] == "-"):
            dropped_columns_names.append(column_name)
            data_without_separation_columns.drop([column_name], inplace=True, axis=1)
    # INFO to check the 'dropped_columns_names' in DGCL .xlsx
    # you can get a column number with the following formula (knowing that you already
    # might have deleted the 1st column): =COLUMN()
    logger.debug(f"Dropped the following columns: {dropped_columns_names}")

    return data_without_separation_columns


def clean_empty_rows_columns(year: int):
    '''
    Remove first empty rows and column and set one column title from the DGCL title and subtitle.
    '''
    if year != 2024:
        logger.warning(
            f"Applying '2024' data cleaning method on '{year}' data but there is nothing like dream to create the future :D")

    raw_criteres_dgcl_path = join(
        FILES_DIRPATH, CRITERES_FILENAME_PREFIX + str(year) + CRITERES_RAW_FILENAME_SUFFIX)
    logger.info(f"Cleaning '{raw_criteres_dgcl_path}' file content...")

    criteres_dgcl_without_2rows: DataFrame = read_csv(
        raw_criteres_dgcl_path, decimal=",", low_memory=False, header=None, skiprows=2)
    logger.info("Loaded raw data extract without 2 top rows: ")
    logger.info(criteres_dgcl_without_2rows.iloc[:, :DEBUG_DISPLAYED_ROWS_NUMBER])

    criteres_dgcl_without_2rows_col1 = criteres_dgcl_without_2rows.iloc[:, 1:]
    logger.debug("Data extract without first column: ")
    logger.debug(criteres_dgcl_without_2rows_col1.iloc[:, :DEBUG_DISPLAYED_ROWS_NUMBER])

    criteres_dgcl_without_2rows_col1_sep = drop_separation_columns(
        criteres_dgcl_without_2rows_col1)

    # for each column, concatenate the values of the 1st and 2nd row and add a
    # hyphen as separator
    concatenated_title_rows: Series = criteres_dgcl_without_2rows_col1_sep.agg(
        '{0[0]} - {0[1]}'.format, axis=0)
    logger.debug(concatenated_title_rows)

    # use concatenated values as new title
    renamed_criteres_dgcl = criteres_dgcl_without_2rows_col1_sep.set_axis(
        concatenated_title_rows, axis=1)

    # remove old titles (2 first rows), reset index and drop old one
    cleaned_criteres_dgcl = renamed_criteres_dgcl.tail(
        -2).reset_index(drop=True)
    logger.info("Final and cleaned data extract: ")
    logger.info(cleaned_criteres_dgcl.iloc[:, :DEBUG_DISPLAYED_ROWS_NUMBER])

    return cleaned_criteres_dgcl


def warn_on_nan(serie):
    if serie.hasnans:
        # TODO: manage outre-mer specificities
        logger.warning(f"Some NaN survived in '{serie.name}'! Indexes to check: {serie.loc[serie.isna()].index.tolist()}")


def from_string_percentage_to_string_float(data_serie: Series):  # -> Series[float]
    return data_serie.replace(r'([0-9]+),([0-9]+) %', r'\1.\2', regex=True)


def set_decimal_to_dot(data_serie: Series, downcast_type: str):
    if downcast_type == 'float':
        data_serie = data_serie.str.replace(',', '.')
    return data_serie


def from_string_numeric_to_numeric(data_serie: Series, downcast_type: str):  # -> Series[int] | Series[float]
    try:
        data_serie = set_decimal_to_dot(data_serie, downcast_type)
        data_serie = to_numeric(data_serie, downcast=downcast_type)
        warn_on_nan(data_serie)
        return data_serie

    except ValueError as ve:
        logger.fatal(f"Value format mismatch. Data extract: {data_serie.head(n=DEBUG_DISPLAYED_ROWS_NUMBER)}\n")
        traceback.print_exc()
        raise ve


def from_string_numeric_with_spaces_to_numeric(data_serie: Series, downcast_type: str):  # -> Series[int] | Series[float]
    # example: 76 543,21 -> 76543.21
    data_serie = set_decimal_to_dot(data_serie, downcast_type)
    data_serie = data_serie.apply(
        lambda s: to_numeric(s.replace(DGCL_VALUE__STRING_NON_BREAKING_SPACE, OUTPUT_VALUE__EMPTY), downcast=downcast_type)
    )
    warn_on_nan(data_serie)
    return data_serie


# naive cleanup to understand the pattern of this 'year' raw data
def clean_data_values(data: DataFrame, year: int):
    # montants (pour les vérifications ou le pluriannuel)

    for dgcl_name in montants_dotations_2024:
        data[dgcl_name] = data[dgcl_name].replace(DGCL_VALUE__NON_COMMUNIQUE, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
        data[dgcl_name] = from_string_numeric_with_spaces_to_numeric(data[dgcl_name], TOUTE_DOTATION_MONTANT_DTYPE_STR)

    column_dsu_garantie_notifiee = "Dotation de solidarité urbaine et de cohésion sociale - Montant de la garantie effectivement appliquée à la commune"
    data[column_dsu_garantie_notifiee] = data[column_dsu_garantie_notifiee].replace(DGCL_VALUE__NON_COMMUNIQUE, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
    data[column_dsu_garantie_notifiee] = from_string_numeric_with_spaces_to_numeric(data[column_dsu_garantie_notifiee], DSU_MONTANT_GARANTIE_NOTIFIEE_DTYPE_STR)

    # critères communs

    column_population_dgf = "Informations générales - Population DGF de l'année N"  # needed by leximpact_dotations_back/data_building/adapt_dotations_criteres.py:ajoute_population_chef_lieu_canton
    data[column_population_dgf] = from_string_numeric_with_spaces_to_numeric(data[column_population_dgf], POPULATION_DGF_DTYPE_STR)

    column_population_insee = "Informations générales - Population INSEE de l'année N"
    data[column_population_insee] = from_string_numeric_with_spaces_to_numeric(data[column_population_insee], POPULATION_INSEE_DTYPE_STR)

    column_superficie = "Informations générales - Superficie de l'année N"
    data[column_superficie] = from_string_numeric_with_spaces_to_numeric(data[column_superficie], SUPERFICIE_DTYPE_STR)

    column_potentiel_financier = "Potentiel fiscal et financier des communes - Potentiel financier final"
    data[column_potentiel_financier] = from_string_numeric_with_spaces_to_numeric(data[column_potentiel_financier], POTENTIEL_FINANCIER_DTYPE_STR)

    column_potentiel_financier_par_habitant = "Potentiel fiscal et financier des communes - Potentiel financier par habitant final"
    data[column_potentiel_financier_par_habitant] = from_string_numeric_with_spaces_to_numeric(data[column_potentiel_financier_par_habitant], POTENTIEL_FINANCIER_PAR_HABITANT_DTYPE_STR)

    column_potentiel_fiscal = "Potentiel fiscal et financier des communes - Potentiel fiscal 4 taxes final"
    if year < 2024:
        data[column_potentiel_fiscal] = data[column_potentiel_fiscal].replace(DGCL_VALUE__NON_DISPONIBLE_BEFORE_2024, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
    else:
        data[column_potentiel_fiscal] = data[column_potentiel_fiscal].replace(DGCL_VALUE__NON_DISPONIBLE_2024, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
    data[column_potentiel_fiscal] = from_string_numeric_with_spaces_to_numeric(data[column_potentiel_fiscal], POTENTIEL_FISCAL_DTYPE_STR)

    column_effort_fiscal = "Effort fiscal - Effort fiscal final"  # 6 decimals
    data[column_effort_fiscal] = data[column_effort_fiscal].replace(DGCL_VALUE__NON_COMMUNIQUE, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
    data[column_effort_fiscal] = from_string_numeric_to_numeric(data[column_effort_fiscal], EFFORT_FISCAL_DTYPE_STR)

    # critères DF

    if year < 2024:  # data previous year
        column_dotation_forfaitaire = "Dotation forfaitaire - Dotation forfaitaire notifiée N"
        data[column_dotation_forfaitaire] = from_string_numeric_with_spaces_to_numeric(data[column_dotation_forfaitaire], DOTATION_FORFAITAIRE_DTYPE_STR_2023)

    column_recettes_reelles_fonctionnement = "Dotation forfaitaire - Recettes réelles de fonctionnement des communes N-2 pour N"
    data[column_recettes_reelles_fonctionnement] = from_string_numeric_with_spaces_to_numeric(data[column_recettes_reelles_fonctionnement], RECETTES_REELLES_FONCTIONNEMENT_DTYPE_STR)

    column_population_dgf_majoree = "Dotation forfaitaire - Population DGF majorée de l'année N"  # 2023 and 2024
    data[column_population_dgf_majoree] = from_string_numeric_with_spaces_to_numeric(data[column_population_dgf_majoree], DF_POPULATION_DGF_MAJOREE_DTYPE_STR)

    # critères DSR

    if year < 2024:  # data previous year
        column_dsr_fraction_cible_part_potentiel_financier_par_habitant = "Dotation de solidarité rurale - Fraction cible - Part Pfi (avant garantie CN)"
        data[column_dsr_fraction_cible_part_potentiel_financier_par_habitant] = data[column_dsr_fraction_cible_part_potentiel_financier_par_habitant].replace(DGCL_VALUE__NON_DISPONIBLE_BEFORE_2024, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
        data[column_dsr_fraction_cible_part_potentiel_financier_par_habitant] = from_string_numeric_with_spaces_to_numeric(data[column_dsr_fraction_cible_part_potentiel_financier_par_habitant], DSR_FRACTION_CIBLE_PART_POTENTIEL_FINANCIER_PAR_HABITANT_DTYPE_STR_2023)

        column_dsr_fraction_cible_part_longueur_voirie = "Dotation de solidarité rurale - Fraction cible - Part VOIRIE (avant garantie CN)"
        data[column_dsr_fraction_cible_part_longueur_voirie] = data[column_dsr_fraction_cible_part_longueur_voirie].replace(DGCL_VALUE__NON_DISPONIBLE_BEFORE_2024, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
        data[column_dsr_fraction_cible_part_longueur_voirie] = from_string_numeric_with_spaces_to_numeric(data[column_dsr_fraction_cible_part_longueur_voirie], DSR_FRACTION_CIBLE_PART_LONGUEUR_VOIRIE_DTYPE_STR_2023)

        column_dsr_fraction_cible_part_enfants = "Dotation de solidarité rurale - Fraction cible - Part ENFANTS (avant garantie CN)"
        data[column_dsr_fraction_cible_part_enfants] = data[column_dsr_fraction_cible_part_enfants].replace(DGCL_VALUE__NON_DISPONIBLE_BEFORE_2024, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
        data[column_dsr_fraction_cible_part_enfants] = from_string_numeric_with_spaces_to_numeric(data[column_dsr_fraction_cible_part_enfants], DSR_FRACTION_CIBLE_PART_ENFANTS_DTYPE_STR_2023)

        column_dsr_fraction_cible_part_potentiel_financier_par_hectare = "Dotation de solidarité rurale - Fraction cible - Part Pfi/hectare ( Pfis) (avant garantie CN)"
        data[column_dsr_fraction_cible_part_potentiel_financier_par_hectare] = data[column_dsr_fraction_cible_part_potentiel_financier_par_hectare].replace(DGCL_VALUE__NON_DISPONIBLE_BEFORE_2024, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
        data[column_dsr_fraction_cible_part_potentiel_financier_par_hectare] = from_string_numeric_with_spaces_to_numeric(data[column_dsr_fraction_cible_part_potentiel_financier_par_hectare], DSR_FRACTION_CIBLE_PART_POTENTIEL_FINANCIER_PAR_HECTARE_DTYPE_STR_2023)

        column_dsr_fraction_cible = "Dotation de solidarité rurale - Fraction cible - Montant global réparti"
        data[column_dsr_fraction_cible] = data[column_dsr_fraction_cible].replace(DGCL_VALUE__NON_DISPONIBLE_BEFORE_2024, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
        data[column_dsr_fraction_cible] = from_string_numeric_with_spaces_to_numeric(data[column_dsr_fraction_cible], DSR_FRACTION_CIBLE_DTYPE_STR_2023)

    column_indice_synthetique = "Dotation de solidarité rurale - Fraction cible - Indice synthétique DSR Cible"  # 6 decimals
    if year < 2024:
        # n.c not detected ; n.d detected
        data[column_indice_synthetique] = data[column_indice_synthetique].replace(DGCL_VALUE__NON_DISPONIBLE_BEFORE_2024, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
    else:
        # n.c detected ; n.d not detected
        data[column_indice_synthetique] = data[column_indice_synthetique].replace(DGCL_VALUE__NON_COMMUNIQUE, OUTPUT_VALUE__EMPTY)
    data[column_indice_synthetique] = from_string_numeric_to_numeric(data[column_indice_synthetique], DSR_C_INDICE_SYNTHETIQUE_DTYPE_STR)

    column_rang_indice_synthetique = "Dotation de solidarité rurale - Fraction cible - Rang DSR Cible"
    if year < 2024:
        # n.c not detected ; n.d detected
        data[column_rang_indice_synthetique] = data[column_rang_indice_synthetique].replace(DGCL_VALUE__NON_DISPONIBLE_BEFORE_2024, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
    else:
        # n.c detected ; n.d not detected
        data[column_rang_indice_synthetique] = data[column_rang_indice_synthetique].replace(DGCL_VALUE__NON_COMMUNIQUE, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
    data[column_rang_indice_synthetique] = from_string_numeric_with_spaces_to_numeric(data[column_rang_indice_synthetique], DSR_C_RANG_INDICE_SYNTHETIQUE_DTYPE_STR)

    if year < 2024:  # data previous year
        column_dsr_fraction_bourg_centre = "Dotation de solidarité rurale - Fraction bourg-centre - Montant global réparti"
        data[column_dsr_fraction_bourg_centre] = data[column_dsr_fraction_bourg_centre].replace(DGCL_VALUE__NON_DISPONIBLE_BEFORE_2024, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
        data[column_dsr_fraction_bourg_centre] = from_string_numeric_with_spaces_to_numeric(data[column_dsr_fraction_bourg_centre], DSR_FRACTION_BOURG_CENTRE_DTYPE_STR_2023)

        column_dsr_montant_eligible_fraction_bourg_centre = "Dotation de solidarité rurale - Fraction bourg-centre - Montant de la commune éligible"
        data[column_dsr_montant_eligible_fraction_bourg_centre] = data[column_dsr_montant_eligible_fraction_bourg_centre].replace(DGCL_VALUE__NON_DISPONIBLE_BEFORE_2024, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
        data[column_dsr_montant_eligible_fraction_bourg_centre] = from_string_numeric_with_spaces_to_numeric(data[column_dsr_montant_eligible_fraction_bourg_centre], DSR_MONTANT_ELIGIBLE_FRACTION_BOURG_CENTRE_DTYPE_STR_2023)

    column_part_population_canton = "Dotation de solidarité rurale - Fraction bourg-centre - Pourcentage de la population communale dans le canton d'appartenance en 2014"
    if year < 2024:
        # n.c not detected ; nd detected
        data[column_part_population_canton] = data[column_part_population_canton].replace(DGCL_VALUE__NON_DISPONIBLE_BEFORE_2024, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
    else:
        # n.c detected ; n.d not detected
        data[column_part_population_canton] = data[column_part_population_canton].replace(DGCL_VALUE__NON_COMMUNIQUE, OUTPUT_VALUE__EMPTY)
    data[column_part_population_canton] = from_string_percentage_to_string_float(data[column_part_population_canton])
    data[column_part_population_canton] = from_string_numeric_to_numeric(data[column_part_population_canton], PART_POPULATION_CANTON_DTYPE_STR)

    column_montant_commune_eligible = "Dotation de solidarité rurale - Fraction bourg-centre - Montant de la commune éligible"
    if year < 2024:
        # n.c not detected ; nd detected
        data[column_montant_commune_eligible] = data[column_montant_commune_eligible].replace(DGCL_VALUE__NON_DISPONIBLE_BEFORE_2024, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
    else:
        # n.c detected ; n.d not detected
        data[column_montant_commune_eligible] = data[column_montant_commune_eligible].replace(DGCL_VALUE__NON_COMMUNIQUE, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
        # string in 2024 (already float in 2023)
        data[column_montant_commune_eligible] = from_string_numeric_with_spaces_to_numeric(data[column_montant_commune_eligible], DSR_BC_MONTANT_COMMUNE_ELIGIBLE_DTYPE_STR)

    column_chef_lieu_departement_dans_agglomeration = "Dotation de solidarité rurale - Fraction bourg-centre - La commune appartient à une UU avec un CL de département ?"  # values : 0 or 1 or n.c
    if year < 2024:
        # n.c not detected ; nd detected
        data[column_chef_lieu_departement_dans_agglomeration] = data[column_chef_lieu_departement_dans_agglomeration].replace(DGCL_VALUE__NON_DISPONIBLE_BEFORE_2024, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
    else:
        # n.c detected ; n.d not detected
        data[column_chef_lieu_departement_dans_agglomeration] = data[column_chef_lieu_departement_dans_agglomeration].replace(DGCL_VALUE__NON_COMMUNIQUE, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
    data[column_chef_lieu_departement_dans_agglomeration] = from_string_numeric_to_numeric(data[column_chef_lieu_departement_dans_agglomeration], DSR_BC_IS_CHEF_LIEU_DEP_AGGLO_DTYPE_STR)

    column_population_dgf_agglomeration = "Dotation de solidarité rurale - Fraction bourg-centre - Population DGF des communes de l'unité urbaine"
    if year < 2024:
        # n.c not detected ; nd detected
        data[column_population_dgf_agglomeration] = data[column_population_dgf_agglomeration].replace(DGCL_VALUE__NON_DISPONIBLE_BEFORE_2024, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
    else:
        # n.c detected ; n.d not detected
        data[column_population_dgf_agglomeration] = data[column_population_dgf_agglomeration].replace(DGCL_VALUE__NON_COMMUNIQUE, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
    data[column_population_dgf_agglomeration] = from_string_numeric_with_spaces_to_numeric(data[column_population_dgf_agglomeration], DSR_BC_POPULATION_DGF_AGGLO_DTYPE_STR)

    column_population_dgf_departement_agglomeration = "Dotation de solidarité rurale - Fraction bourg-centre - Population départementale de référence de l'unité urbaine"
    if year < 2024:
        # n.c not detected ; nd detected
        data[column_population_dgf_departement_agglomeration] = data[column_population_dgf_departement_agglomeration].replace(DGCL_VALUE__NON_DISPONIBLE_BEFORE_2024, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
    else:
        # n.c detected ; n.d not detected
        data[column_population_dgf_departement_agglomeration] = data[column_population_dgf_departement_agglomeration].replace(DGCL_VALUE__NON_COMMUNIQUE, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
    data[column_population_dgf_departement_agglomeration] = from_string_numeric_with_spaces_to_numeric(data[column_population_dgf_departement_agglomeration], DSR_BC_POPULATION_DGF_DEP_AGGLO_DTYPE_STR)

    if year < 2024:  # data previous year
        column_dsr_fraction_perequation = "Dotation de solidarité rurale - Fraction péréquation - Montant global réparti (après garantie CN)"
        data[column_dsr_fraction_perequation] = data[column_dsr_fraction_perequation].replace(DGCL_VALUE__NON_DISPONIBLE_BEFORE_2024, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
        data[column_dsr_fraction_perequation] = from_string_numeric_with_spaces_to_numeric(data[column_dsr_fraction_perequation], DSR_FRACTION_PEREQUATION_DTYPE_STR_2023)

        column_dsr_fraction_perequation_part_potentiel_financier_par_habitant = "Dotation de solidarité rurale - Fraction péréquation - Part Pfi (avant garantie CN)"
        data[column_dsr_fraction_perequation_part_potentiel_financier_par_habitant] = data[column_dsr_fraction_perequation_part_potentiel_financier_par_habitant].replace(DGCL_VALUE__NON_DISPONIBLE_BEFORE_2024, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
        data[column_dsr_fraction_perequation_part_potentiel_financier_par_habitant] = from_string_numeric_with_spaces_to_numeric(data[column_dsr_fraction_perequation_part_potentiel_financier_par_habitant], DSR_FRACTION_PEREQUATION_PART_POTENTIEL_FINANCIER_PAR_HABITANT_DTYPE_STR_2023)

        column_dsr_fraction_perequation_part_longueur_voirie = "Dotation de solidarité rurale - Fraction péréquation - Part VOIRIE (avant garantie CN)"
        data[column_dsr_fraction_perequation_part_longueur_voirie] = data[column_dsr_fraction_perequation_part_longueur_voirie].replace(DGCL_VALUE__NON_DISPONIBLE_BEFORE_2024, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
        data[column_dsr_fraction_perequation_part_longueur_voirie] = from_string_numeric_with_spaces_to_numeric(data[column_dsr_fraction_perequation_part_longueur_voirie], DSR_FRACTION_PEREQUATION_PART_LONGUEUR_VOIRIE_DTYPE_STR_2023)

        column_dsr_fraction_perequation_part_enfants = "Dotation de solidarité rurale - Fraction péréquation - Part ENFANTS (avant garantie CN)"
        data[column_dsr_fraction_perequation_part_enfants] = data[column_dsr_fraction_perequation_part_enfants].replace(DGCL_VALUE__NON_DISPONIBLE_BEFORE_2024, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
        data[column_dsr_fraction_perequation_part_enfants] = from_string_numeric_with_spaces_to_numeric(data[column_dsr_fraction_perequation_part_enfants], DSR_FRACTION_PEREQUATION_PART_ENFANTS_DTYPE_STR_2023)

        column_dsr_fraction_perequation_part_potentiel_financier_par_hectare = "Dotation de solidarité rurale - Fraction péréquation - Part Pfi/hectare (avant garantie CN)"
        data[column_dsr_fraction_perequation_part_potentiel_financier_par_hectare] = data[column_dsr_fraction_perequation_part_potentiel_financier_par_hectare].replace(DGCL_VALUE__NON_DISPONIBLE_BEFORE_2024, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
        data[column_dsr_fraction_perequation_part_potentiel_financier_par_hectare] = from_string_numeric_with_spaces_to_numeric(data[column_dsr_fraction_perequation_part_potentiel_financier_par_hectare], DSR_FRACTION_PEREQUATION_PART_POTENTIEL_FINANCIER_PAR_HECTARE_DTYPE_STR_2023)

    column_part_pfi = "Dotation de solidarité rurale - Fraction péréquation - Part Pfi" if year == 2024 else "Dotation de solidarité rurale - Fraction péréquation - Part Pfi (avant garantie CN)"
    if year < 2024:
        # n.c not detected ; nd detected
        data[column_part_pfi] = data[column_part_pfi].replace(DGCL_VALUE__NON_DISPONIBLE_BEFORE_2024, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
    else:
        # n.c detected ; n.d not detected
        data[column_part_pfi] = data[column_part_pfi].replace(DGCL_VALUE__NON_COMMUNIQUE, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
        # string in 2024 (not in 2023)
        data[column_part_pfi] = from_string_numeric_with_spaces_to_numeric(data[column_part_pfi], DSR_PRQ_PART_PFI_DTYPE_STR)

    column_longueur_voirie = "Dotation de solidarité rurale - Fraction péréquation - Longueur de voirie en mètres"
    if year < 2024:
        # n.c not detected ; nd detected
        data[column_longueur_voirie] = data[column_longueur_voirie].replace(DGCL_VALUE__NON_DISPONIBLE_BEFORE_2024, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
    else:
        # n.c detected ; n.d not detected
        data[column_longueur_voirie] = data[column_longueur_voirie].replace(DGCL_VALUE__NON_COMMUNIQUE, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
    data[column_longueur_voirie] = from_string_numeric_with_spaces_to_numeric(data[column_longueur_voirie], DSR_PRQ_LONGUEUR_VOIRIE_DTYPE_STR)

    column_population_enfants = "Dotation de solidarité rurale - Fraction péréquation - Population 3 à 16 ans"
    if year < 2024:
        # n.c not detected ; nd detected
        data[column_population_enfants] = data[column_population_enfants].replace(DGCL_VALUE__NON_DISPONIBLE_BEFORE_2024, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
    else:
        # n.c detected ; n.d not detected
        data[column_population_enfants] = data[column_population_enfants].replace(DGCL_VALUE__NON_COMMUNIQUE, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
    data[column_population_enfants] = from_string_numeric_with_spaces_to_numeric(data[column_population_enfants], DSR_PRQ_POPULATION_ENFANTS_DTYPE_STR)

    # critères DSU

    if year < 2024:  # data previous year
        column_dsu_part_spontanee = "Dotation de solidarité urbaine et de cohésion sociale - Montant attribution spontanée DSU"
        data[column_dsu_part_spontanee] = from_string_numeric_with_spaces_to_numeric(data[column_dsu_part_spontanee], DSU_PART_SPONTANEE_DTYPE_STR_2023)

        column_dsu_part_augmentation = "Dotation de solidarité urbaine et de cohésion sociale - Montant progression de la DSU"
        data[column_dsu_part_augmentation] = from_string_numeric_with_spaces_to_numeric(data[column_dsu_part_augmentation], DSU_PART_AUGMENTATION_DTYPE_STR_2023)

        column_dsu_montant = "Dotation de solidarité urbaine et de cohésion sociale - Montant total réparti"
        data[column_dsu_montant] = from_string_numeric_with_spaces_to_numeric(data[column_dsu_montant], DSU_MONTANT_DTYPE_STR_2023)

    column_revenu_total = "Dotation de solidarité urbaine et de cohésion sociale - Revenu imposable des habitants de la commune"
    if year < 2024:
        data[column_revenu_total] = data[column_revenu_total].replace(DGCL_VALUE__NON_COMMUNIQUE, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR).replace(DGCL_VALUE__NON_DISPONIBLE_BEFORE_2024, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
    else:
        data[column_revenu_total] = data[column_revenu_total].replace(DGCL_VALUE__NON_COMMUNIQUE, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR).replace(DGCL_VALUE__NON_DISPONIBLE_2024, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
    data[column_revenu_total] = from_string_numeric_with_spaces_to_numeric(data[column_revenu_total], REVENU_TOTAL_DTYPE_STR)

    column_nombre_beneficiaires_aides_au_logement = "Dotation de solidarité urbaine et de cohésion sociale - Nombre de bénéficiaires des aides au logement de la commune"
    if year < 2024:
        # n.c not detected ; nd detected
        data[column_nombre_beneficiaires_aides_au_logement] = data[column_nombre_beneficiaires_aides_au_logement].replace(DGCL_VALUE__NON_DISPONIBLE_BEFORE_2024, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
    else:
        # n.c detected ; n.d not detected
        data[column_nombre_beneficiaires_aides_au_logement] = data[column_nombre_beneficiaires_aides_au_logement].replace(DGCL_VALUE__NON_COMMUNIQUE, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
    data[column_nombre_beneficiaires_aides_au_logement] = from_string_numeric_with_spaces_to_numeric(data[column_nombre_beneficiaires_aides_au_logement], DSU_NOMBRE_BENEFICIAIRES_AIDES_LOGEMENT_DTYPE_STR)

    column_nombre_logements = "Dotation de solidarité urbaine et de cohésion sociale - Nombre de logements TH de la commune"
    data[column_nombre_logements] = data[column_nombre_logements].replace(DGCL_VALUE__NON_COMMUNIQUE, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
    data[column_nombre_logements] = from_string_numeric_with_spaces_to_numeric(data[column_nombre_logements], DSU_NOMBRE_LOGEMENTS_DTYPE_STR)

    column_nombre_logements_sociaux = "Dotation de solidarité urbaine et de cohésion sociale - Nombre de logements sociaux de la commune"
    if year < 2024:
        # n.c not detected ; nd detected
        data[column_nombre_logements_sociaux] = data[column_nombre_logements_sociaux].replace(DGCL_VALUE__NON_DISPONIBLE_BEFORE_2024, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
    else:
        # n.c detected ; n.d not detected
        data[column_nombre_logements_sociaux] = data[column_nombre_logements_sociaux].replace(DGCL_VALUE__NON_COMMUNIQUE, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
    data[column_nombre_logements_sociaux] = from_string_numeric_with_spaces_to_numeric(data[column_nombre_logements_sociaux], DSU_NOMBRE_LOGEMENTS_SOCIAUX_DTYPE_STR)

    column_population_qpv = "Dotation de solidarité urbaine et de cohésion sociale - Population QPV"
    if year < 2024:
        # n.c not detected ; nd detected
        data[column_population_qpv] = data[column_population_qpv].replace(DGCL_VALUE__NON_DISPONIBLE_BEFORE_2024, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
    else:
        # n.c detected ; n.d not detected
        data[column_population_qpv] = data[column_population_qpv].replace(DGCL_VALUE__NON_COMMUNIQUE, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
    data[column_population_qpv] = from_string_numeric_with_spaces_to_numeric(data[column_population_qpv], DSU_POPULATION_QPV_DTYPE_STR)

    column_population_zfu = "Dotation de solidarité urbaine et de cohésion sociale - Population ZFU"
    if year < 2024:
        # n.c not detected ; nd detected
        data[column_population_zfu] = data[column_population_zfu].replace(DGCL_VALUE__NON_DISPONIBLE_BEFORE_2024, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
    else:
        # n.c detected ; n.d not detected
        data[column_population_zfu] = data[column_population_zfu].replace(DGCL_VALUE__NON_COMMUNIQUE, OUTPUT_VALUE__NEUTRAL_NUMERICAL_AS_STR)
    data[column_population_zfu] = from_string_numeric_with_spaces_to_numeric(data[column_population_zfu], DSU_POPULATION_ZFU_DTYPE_STR)

    return data


def create_criteres_dgcl_csv(clean_criteres: DataFrame, year: int):
    new_criteres_csv_path = join(
        FILES_DIRPATH, OUTPUT_RELATIVE_DIRPATH, CRITERES_FILENAME_PREFIX + str(year) + ".csv")

    logger.info(f"Creating {new_criteres_csv_path} file...")
    clean_criteres.to_csv(new_criteres_csv_path, index=False)


if __name__ == '__main__':
    nb_command_arguments = len(sys.argv)

    if nb_command_arguments == 1:
        year = 2024
        logger.info(f"You didn't ask for a specific year. Cleaning default '{year}' data...")
    else:
        year = int(sys.argv[1])
        logger.info(f"Cleaning '{year}' data...")

    compact_criteres_dgcl_year = clean_empty_rows_columns(year)
    cleaned_criteres_dgcl_year = clean_data_values(compact_criteres_dgcl_year, year)
    create_criteres_dgcl_csv(cleaned_criteres_dgcl_year, year)
