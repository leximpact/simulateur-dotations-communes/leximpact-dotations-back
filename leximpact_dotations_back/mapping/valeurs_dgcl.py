DGCL_VALUE__NON_COMMUNIQUE = "n.c"  # meaning to be confirmed (non concerné ? non connu ? non chiffrable ?)

DGCL_VALUE__NON_DISPONIBLE_BEFORE_2024 = "nd"  # meaning to be confirmed
DGCL_VALUE__NON_DISPONIBLE_2024 = "n.d"  # meaning to be confirmed
DGCL_VALUE__STRING_NON_BREAKING_SPACE = "\xa0"

DGCL_VALUE_STRING_TRUE_BEFORE_2024 = "OUI"
DGCL_VALUE_STRING_TRUE_PATTERN_BEFORE_2024 = "oui"
DGCL_VALUE_STRING_TRUE = "1"  # 2024 and some before?
DGCL_VALUE_STRING_FALSE = "0"  # 2024 and some before?
