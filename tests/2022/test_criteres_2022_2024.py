from os.path import dirname, join

from leximpact_dotations_back.data_building.build_dotations_data import DATA_DIRECTORY, load_criteres


def test_criteres_columns_2022_2024():
    test_data_dirpath = join(dirname(__file__), "data")

    criteres_data_2022 = load_criteres(test_data_dirpath, 2022)
    criteres_data_2024 = load_criteres(DATA_DIRECTORY, 2024)
    print(criteres_data_2022.columns)

    assert len(criteres_data_2022.columns) == 205
    assert len(criteres_data_2024.columns) == 180

    criteres_indexes_2024_2022_diff = criteres_data_2024.columns.difference(criteres_data_2022.columns)
    assert len(criteres_indexes_2024_2022_diff) >= (205 - 180)  # more than 25
