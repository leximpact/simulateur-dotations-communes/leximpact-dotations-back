import pytest
from leximpact_dotations_back.data_building.build_dotations_data import DATA_DIRECTORY, load_criteres
from leximpact_dotations_back.data_building.adapt_dotations_criteres import adapt_criteres


@pytest.fixture
def data_2024():
    return load_criteres(DATA_DIRECTORY, 2024)


def test_adapt_criteres_2024(data_2024):
    adapted_data_2024 = adapt_criteres(data_2024, 2024)

    assert adapted_data_2024["chef_lieu_departement_dans_agglomeration"].dtype == 'bool'  # data_cleanup.py + adapt_criteres(...)
    assert adapted_data_2024["bureau_centralisateur"].dtype == 'bool'  # adapt_criteres(...)
