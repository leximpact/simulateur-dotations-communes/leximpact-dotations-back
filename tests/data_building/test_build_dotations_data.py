from leximpact_dotations_back.data_building.build_dotations_data import build_data


def test_build_data_2024():
    # src: https://fr.wikipedia.org/wiki/Nombre_de_communes_en_France#Année_2024
    nb_communes__metropole_2024 = 34_806

    # DROM = Guadeloupe, La Réunion + CTU (Martinique, Guyane, Mayotte)
    # COM = Polynésie française, Saint-Barthélemy, Saint-Martin, Saint-Pierre-et-Miquelon, Wallis-et-Futuna
    nb_communes__drom_2024 = 129
    nb_communes__com__nouvelle_caledonie = 94

    # TODO: delta à clarifier ; 11 communes nouvelles, mais 22 à 24 regroupées, 6 renommées...
    delta = -8

    dotations_data_2024 = build_data(2024)

    assert dotations_data_2024.shape[0] == (nb_communes__metropole_2024 + nb_communes__drom_2024 + nb_communes__com__nouvelle_caledonie + delta)
