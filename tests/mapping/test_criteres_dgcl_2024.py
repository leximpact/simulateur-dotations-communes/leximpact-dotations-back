import pytest

from leximpact_dotations_back.data_building.build_dotations_data import DATA_DIRECTORY, load_criteres
from leximpact_dotations_back.mapping.criteres_dgcl_2024 import commune_columns_to_keep_2024, variables_calculees_presentes_2024, colonnes_utiles_2024


# INFO Here "cleaned data" are raw DGCL critères data loaded to a DataFrame thanks to python data/raw/data_cleanup.py


@pytest.fixture
def cleaned_criteres_data_2024():
    return load_criteres(DATA_DIRECTORY, 2024)


def test_all_columns_exist_in_cleaned_data(cleaned_criteres_data_2024):
    data_columns_2024 = cleaned_criteres_data_2024.columns

    for column_name in commune_columns_to_keep_2024:
        assert column_name in data_columns_2024, f"Missing this column: {column_name}"


def test_auxiliary_columns_exist_in_cleaned_data(cleaned_criteres_data_2024):
    data_columns_2024 = cleaned_criteres_data_2024.columns

    for data_column_name_as_index, openfisca_variable in variables_calculees_presentes_2024.items():
        assert data_column_name_as_index in data_columns_2024, f"Missing this column from 'variables_calculees_presentes_2024': '{data_column_name_as_index}'"

    for openfisca_variable_as_index, data_column_name in colonnes_utiles_2024.items():
        if data_column_name != "":
            assert data_column_name in data_columns_2024, f"Missing column from 'colonnes_utiles_2024': '{data_column_name}'"
