import pytest

from numpy import array_equal, zeros

from openfisca_france_dotations_locales import CountryTaxBenefitSystem as OpenFiscaFranceDotationsLocales

from leximpact_dotations_back.data_building.build_dotations_data import build_data
from leximpact_dotations_back.calculate import create_simulation_with_data


@pytest.fixture
def model():
    return OpenFiscaFranceDotationsLocales()


@pytest.fixture
def data():
    return build_data(2024)


def test_create_simulation_with_data(model, data):
    period = 2024
    nombre_communes_period = data.shape[0]

    simulation = create_simulation_with_data(model, period, data)

    dotation_forfaitaire = simulation.calculate('dotation_forfaitaire', period)
    dotation_solidarite_rurale = simulation.calculate('dotation_solidarite_rurale', period)
    dsu_montant = simulation.calculate('dsu_montant', period)

    assert len(dotation_forfaitaire) == nombre_communes_period
    assert not array_equal(dotation_forfaitaire, zeros(nombre_communes_period))  # NaN not accepted here

    assert len(dotation_solidarite_rurale) == nombre_communes_period
    assert not array_equal(dotation_solidarite_rurale, zeros(nombre_communes_period))  # NaN not accepted here

    assert len(dsu_montant) == nombre_communes_period
    # TODO fix result ; shouldn't be zeros
    # assert not array_equal(dsu_montant , zeros(nombre_communes_period))    # NaN not accepted here
