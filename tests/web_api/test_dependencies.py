import logging
from fastapi.testclient import TestClient
from leximpact_dotations_back.main import app


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)
client = TestClient(app)


def test_dependencies():
    response = client.get("/dependencies")
    assert response.status_code == 200  # OK
    logger.debug(response.json())
    assert "OpenFisca-France-Dotations-Locales" in response.json().keys()
